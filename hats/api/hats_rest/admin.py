from django.contrib import admin
from .models import LocationVO, Hat

@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    pass

@admin.register(Hat)
class HatsAdmin(admin.ModelAdmin):
    pass
