import { React, useEffect, useState } from 'react';


function HatForm () {
    const [locations, setLocations] = useState([]);
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [url, setUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleUrlChange = (event) => {
        const value = event.target.value;
        setUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            "style_name": styleName,
            color,
            fabric,
            url,
            location,
        }

        const hatUrl = `http://localhost:8090/api/location/${location}/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch (hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            setColor('');
            setStyleName('');
            setFabric('');
            setUrl('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleStyleNameChange} value={styleName} placeholder="StyleName" required type="text" name="styleName" id="styleName" className="form-control"/>
                  <label htmlFor="styleName">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="Fabric" id="Fabric" className="form-control"/>
                  <label htmlFor="Fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleUrlChange} value={url} placeholder="Url" required type="url" name="url" id="url" className="form-control"/>
                  <label htmlFor="url">URL</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} value={location} required id="Location" name="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.closet_name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}
export default HatForm
