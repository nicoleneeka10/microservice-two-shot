from django.db import models

class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=100, null=True)


class Shoe(models.Model):
    manufacturer_name = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    shoe_picture_url = models.URLField(max_length=200)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT,
    )
