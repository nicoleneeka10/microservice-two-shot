# Generated by Django 4.0.3 on 2023-04-19 23:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_rename_hat_picture_url_hats_url_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hats',
            name='fabric',
        ),
    ]
