import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatList from './HatList';
import React from 'react';
import ShoeList from './ShoeList'
import ShoeForm from './ShoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
              <Route index element={<ShoeList/>} />
              <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
