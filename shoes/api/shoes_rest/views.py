from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["bin_number", "closet_name"]


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer_name",
        "model_name",
        "color",
        "shoe_picture_url",
        "id",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


# class ShoeListEncoder(ModelEncoder):
#     model = Shoe
#     properties = [
#         "manufacturer_name"

#     ]
#     def get_extra_data(self, o):
#         bin = o.bin
#         bin_number = bin.bin_number
#         return {"bin": bin_number}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoepair = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoepair},
            encoder=ShoeEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            bin_number = content["bin"]
            bin = BinVO.objects.get(bin_number=bin_number)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin number!"},
                status=400,
            )
        shoepair = Shoe.objects.create(**content)
        print(shoepair)
        return JsonResponse(
            shoepair,
            encoder=ShoeEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_shoe(request, pk):
    try:
        shoe = Shoe.objects.get(id=pk)
        shoe.delete()
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    except Shoe.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
