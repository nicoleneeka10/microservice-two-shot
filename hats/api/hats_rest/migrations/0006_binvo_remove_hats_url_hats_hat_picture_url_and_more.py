# Generated by Django 4.0.3 on 2023-04-20 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_hats_fabric'),
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=100)),
                ('bin_number', models.PositiveSmallIntegerField()),
                ('bin_size', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='hats',
            name='url',
        ),
        migrations.AddField(
            model_name='hats',
            name='hat_picture_url',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='hats',
            name='color',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='hats',
            name='fabric',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='hats',
            name='style_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
