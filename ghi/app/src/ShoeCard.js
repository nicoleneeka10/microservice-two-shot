import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ShoeCard(props) {
    if (props.shoe === undefined) {
        return null;
      }

    return (
        <>
        <div className="col">
            <div key={props.shoe.id} className="card mb-3 shadow">
                <img src={props.shoe.shoe_picture_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">Model name: {props.shoe.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    Manufacturer: {props.shoe.manufacturer_name}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                    Color: {props.shoe.color}
                </h6>
                </div>
                <div className="card-footer">
                    Closet: {props.shoe.bin.closet_name} |
                    Bin: {props.shoe.bin.bin_number}
                </div>
                <div className="card-footer">
                <button onClick={props.deleteShoe} type="button" className="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
        </>

    )

}

export default ShoeCard
