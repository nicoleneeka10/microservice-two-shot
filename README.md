# Wardrobify

Team:

* Daniel Ahdoot - Hats
* Nicole Kash - Shoes


## Design

## Shoes microservice

I have two models, the Shoe model and the BinVO model. The BinVO model is integrated
with the wardrobe microservice via polling. My poller polls for new bins created in
the wardrobe microservice and then creates or updates BinVO objects.

My BinVO objects have a bin number and closet name as I did not feel bin size was important
for this specific VO object. To identify BinVO objects, I used the bin number and NOT the Django
generated bin id. I did this to prevent syncing related issues in the event a bin is deleted
from the wardrobe microservice.



## Hats microservice
The hat and locationVO model was created and there can be multiple hats for one location.
Since the location data is stored in the wardrobe micro-service, the hats microservice should be unable to make changes to the Location data from the wardrobe microservice. Therefore, a polling microservice was made to create instances of the LocationVO for the hat microservice.
The models and wardrobe microservice was integrated together to create a single-page application using React by implementing RESTful apis for getting, creating, and deleting hats.

