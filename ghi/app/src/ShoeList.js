import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import ShoeCard from './ShoeCard';

function ShoeList() {
    const [shoeCards, setShoeCards] = useState([]);
    const deleteShoe = async (index, id) => {
        const shoeURL = 'http://localhost:8080/api/shoes/' + id
        const fetchConfig = {
            method: "delete",

          };


        const response = await fetch(shoeURL, fetchConfig);
        if (response.ok) {
            const Something= shoeCards.filter(function (currentElement, i) {
                return i !== index;
            })

            setShoeCards(Something);


        }



    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';


        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoeCards(data.shoes)

        }





    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
        <div className="container mt-4">
        <h2>A list of shoes</h2>
        <div className="row row-cols-3">
          {shoeCards.map((shoeCard, index) => {
            return (
              <ShoeCard key={index} index={index} shoe={shoeCard} updateState={setShoeCards} deleteShoe={()=>deleteShoe(index, shoeCard.id)} shoeCardsList={shoeCards}/>
            );
          })}
        </div>
      </div>

        </>
    )



}


export default ShoeList
